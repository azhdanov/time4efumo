package jira.plugin.report.pbs;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.log4j.Logger;
import org.ofbiz.core.entity.EntityExpr;
import org.ofbiz.core.entity.EntityOperator;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.entity.GenericValue;
import org.ofbiz.core.util.UtilMisc;

import com.atlassian.core.ofbiz.CoreFactory;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.comparator.IssueKeyComparator;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverterImpl;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.plugin.report.impl.AbstractReport;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.DateFieldFormatImpl;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.ParameterUtils;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.ProjectActionSupport;
import com.atlassian.jira.web.bean.I18nBean;
import com.fdu.jira.plugin.report.pivot.Pivot;
import com.fdu.jira.util.MyFullNameComparator;
import com.fdu.jira.util.PbsUser;
import com.fdu.jira.util.TextUtil;
import com.osoboo.jira.metadata.MetadataService;

/**
 * 
 * @author Andriy Zhdanov (azhdanov@gmail.com)
 *
 */
public class PbsReport extends AbstractReport {
    private static final Logger log = Logger.getLogger(PbsReport.class);

    private static final String PROPERTY_PREFIX = "jira.plugin.report.pbs.";

    public static final String SYS_ANALYSTS_JIRA_GRP = "System analysts";

    private static final String SYS_ANALYSTS_PBS_GRP = "System analytics";

    public static final String TEST_ENG_JIRA_GRP = "Test engineers";

    private static final String TEST_ENG_PBS_GRP = "Test engineers";

    public static final String DESIGNERS_JIRA_GRP = "Designers";

    private static final String DESIGNERS_PBS_GRP = "Design";

    public static final String DEV_JIRA_GRP = "Developers";

    public static final String JR_DEV_JIRA_GRP = "Junior Developers";

    private static final String DEV_PBS_GRP = "Developers";

    public static final String HTML_CODERS_JIRA_GRP = "HTML coders";

    private static final String HTML_CODERS_PBS_GRP = "HTML Developers";

    public static final String COPYWRITERS_JIRA_GRP = "Copywriters";

    private static final String COPYWRITERS_PBS_GRP = COPYWRITERS_JIRA_GRP;

    private static final String UNKNOWN_PBS_GRP = "N/A";

    private static final String PROJECT_TECH_LEAD_ROLE_NAME = "Lead project PHP developer";

    private static final String BILLING_STATUS_AGREEMENT = "agreement";

    private static final String BILLING_STATUS_FACTUAL = "factual";
    
    private static final String BILLING_STATUS_FREE = "free";

    public Map<Issue, Map<PbsUser, Long>> workedUsersOrig =
            new TreeMap<Issue, Map<PbsUser, Long>>(new IssueKeyComparator());

    public Map<Issue, Map<PbsUser, Long>> workedUsers =
            new TreeMap<Issue, Map<PbsUser, Long>>(new IssueKeyComparator());

    public Map<Issue, Map<String, Long>> workedGroups =
            new TreeMap<Issue, Map<String, Long>>(new IssueKeyComparator());

    public Map<Issue, Map<PbsUser, Double>> pbsUsers =
            new TreeMap<Issue, Map<PbsUser, Double>>(new IssueKeyComparator());

    public Map<PbsUser, Long> workedTotal =
            new TreeMap<PbsUser, Long>(new MyFullNameComparator<PbsUser>());

    
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectManager projectManager; 
    private final WorklogManager worklogManager;
    private final ProjectRoleManager projectRoleManager;
    private final UserUtil userUtil;
    private final TimeZoneManager timeZoneManager;
    private final FieldManager fieldManager;
    private final ApplicationProperties applicationProperties;
    private final DateTimeFormatterFactory dateTimeFormatterFactory;
    private final FieldVisibilityManager fieldVisibilityManager;

    private final MetadataService metadataService;

    private PbsUser auditLeadUser;
    private PbsUser itLeadUser;

    private CustomField auditLeadPbsField;
    private CustomField itLeadPbsField;
    private CustomField projectLeadPbsField;
    private CustomField projectTechLeadPbsField;
    private CustomField billingStatusField;

    private ProjectRole techLeadRole;

    private Map<String, CustomField> pbsGroupToField = new Hashtable<String, CustomField>();
    private Map<String, PbsUser> osUserToPbsUser = new Hashtable<String, PbsUser>();
    private Map<Project, Collection<PbsUser>> projectToTechLeads = new Hashtable<Project, Collection<PbsUser>>();


    public PbsReport(JiraAuthenticationContext authenticationContext,
            ProjectManager projectManager,
            WorklogManager worklogManager,
            ProjectRoleManager projectRoleManager,
            UserUtil userUtil,
            TimeZoneManager timeZoneManager,
            FieldManager fieldManager,
            ApplicationProperties applicationProperties,
            DateTimeFormatterFactory dateTimeFormatterFactory,
            FieldVisibilityManager fieldVisibilityManager) {
        this.authenticationContext = authenticationContext;
        this.projectManager = projectManager;
        this.worklogManager = worklogManager;
        this.projectRoleManager = projectRoleManager;
        this.userUtil = userUtil;
        this.timeZoneManager = timeZoneManager;
        this.fieldManager = fieldManager;
        this.applicationProperties = applicationProperties;
        this.dateTimeFormatterFactory = dateTimeFormatterFactory;
        this.fieldVisibilityManager = fieldVisibilityManager;

        // TODO: reflection to diagnose missing plugin gracefully
        metadataService = ComponentManager.getOSGiComponentInstanceOfType(MetadataService.class);
    }

    public void getTimeSpents(User targetUser, Date startDate, Date endDate,
            Long projectId, Long filterId, String targetGroup, boolean excelView) throws SearchException,
            GenericEntityException {

        EntityExpr startExpr = new EntityExpr("startdate",
                EntityOperator.GREATER_THAN_EQUAL_TO, new Timestamp(
                        startDate.getTime()));
        EntityExpr endExpr = new EntityExpr("startdate",
                EntityOperator.LESS_THAN, new Timestamp(endDate.getTime()));
        List<EntityExpr> exprs = UtilMisc.toList(startExpr, endExpr);


        List<GenericValue> worklogs = CoreFactory.getGenericDelegator().findByAnd(
                "Worklog", exprs);
        log.info("Query since '" + startDate + "', till '" + endDate + "' returned: " + worklogs.size() + " worklogs");
        processWorklogs(worklogs);
    }

    public void processWorklogs(List<GenericValue> worklogs) {
        // setup
        techLeadRole = projectRoleManager.getProjectRole(PROJECT_TECH_LEAD_ROLE_NAME);
        if (techLeadRole == null) {
            log.error("No Project Role '" + PROJECT_TECH_LEAD_ROLE_NAME + "'");
        }
        setupFields();
        setupPbsUsers();

        for (GenericValue genericWorklog : worklogs) {
            Worklog worklog = worklogManager.getById(genericWorklog.getLong("id"));
            Issue issue = worklog.getIssue();
            Issue issueOrig = issue;
            if (issue.isSubTask()) {
                Issue parentIssue = issue.getParentObject();
                String billingStatus = (String) parentIssue.getCustomFieldValue(billingStatusField);
                if (BILLING_STATUS_AGREEMENT.equals(billingStatus)) {
                    issue = parentIssue; // count worked hours to parent 
                } else if (BILLING_STATUS_FREE.equals(billingStatus)) {
                   continue; // skip
                }                
            }

            // html view shows summary hours per issue for each user
            Map<PbsUser, Long> userWorkLog = getWorkedUsers(workedUsers, issue);
            Map<PbsUser, Long> userWorkLogOrig = getWorkedUsers(workedUsersOrig, issueOrig);

            Map<String, Long> groupWorkLog = workedGroups.get(issue);
            if (groupWorkLog == null) {
                groupWorkLog = new Hashtable<String, Long>();
                workedGroups.put(issue, groupWorkLog);
            }

            // user per issue
            PbsUser user;
            if (worklog.getAuthor() != null) {
                User osuser = userUtil.getUserObject(worklog
                    .getAuthor());
                if (osuser != null) {
                    user = osUserToPbsUser(osuser);
                } else {
                    // TIME-221: user may have been deleted
                    user = new PbsUser("deleted", "deleted", UNKNOWN_PBS_GRP);
                }
            } else {
                user = new PbsUser("anonymous", "anonymous", UNKNOWN_PBS_GRP);
            }

            addTimeSpent(worklog, user, userWorkLog);
            addTimeSpent(worklog, user, userWorkLogOrig);
            String pbsGroup = user.getPbsGroup();
            addTimeSpent(worklog, pbsGroup, groupWorkLog);
        }

        // PBS
        for (Issue issue : workedUsers.keySet()) {
            Map<PbsUser, Long> userWorkLog = workedUsers.get(issue);
            Map<String, Long> groupWorkLog = workedGroups.get(issue);
            Map<PbsUser, Double> userPbs = pbsUsers.get(issue);
            if (userPbs == null) { // always null?
                userPbs = new Hashtable<PbsUser, Double>();
                pbsUsers.put(issue, userPbs);
            }
            for (PbsUser user : userWorkLog.keySet()) {
                long userWorked = userWorkLog.get(user);
                long groupWorked = groupWorkLog.get(user.getPbsGroup());
                double pbsRate = pbsForUser(user, issue); 
                double pbsAdd = pbsRate * userWorked / groupWorked;
                //System.out.println(issue.getKey() + ":" + user.getName() + ":" + String.format("%.1f", pbsAdd));
                Double pbs = userPbs.get(user);
                if (pbs != null) {
                    pbsAdd += pbs; 
                }
                userPbs.put(user, pbsAdd);

                Long userTotal = workedTotal.get(user);
                if (userTotal != null) {
                    userWorked += userTotal;
                }
                workedTotal.put(user, userWorked);
            }
            // Leads additionally get PBS per every worked issue 
            addLeadsPbs(issue, userPbs);
        }
    }

    private <T> void addTimeSpent(Worklog worklog, T key, Map<T, Long> workLogMap) {
        long timespent = worklog.getTimeSpent();
        Long worked = workLogMap.get(key);
        if (worked != null) {
            timespent += worked;
        }
        workLogMap.put(key, timespent);
    }

    private Map<PbsUser, Long> getWorkedUsers(
            Map<Issue, Map<PbsUser, Long>> map, Issue issue) {
        Map<PbsUser, Long> userWorkLog = map.get(issue);
        if (userWorkLog == null) {
            userWorkLog = new Hashtable<PbsUser, Long>();
            map.put(issue, userWorkLog);
        }
        return userWorkLog;
    }

    private PbsUser osUserToPbsUser(User osuser) {
        PbsUser pbsUser = osUserToPbsUser.get(osuser.getName());
        if (pbsUser == null) {
            String pbsGroup = userToPbsGroup(osuser.getName());
            pbsUser = new PbsUser(osuser.getName(), osuser.getDisplayName(), pbsGroup);
            osUserToPbsUser.put(osuser.getName(), pbsUser);
        }
        return pbsUser;
    }

    private double pbsForUser(PbsUser user, Issue issue) {
        CustomField pbsField = pbsGroupToField.get(user.getPbsGroup());
        if (pbsField == null) {
            log.error("No PBS for user/group '" + user.getName() + "/" + user.getPbsGroup() + "'");
            return 0D;
        }
        return getPbs(issue, pbsField);
    }


    private void addLeadsPbs(Issue issue, Map<PbsUser, Double> userPbs) {
        addLeadPbs(auditLeadUser, auditLeadPbsField, issue, userPbs);
        addLeadPbs(itLeadUser, itLeadPbsField, issue, userPbs);

        User osuser = issue.getProjectObject().getLeadUser();
        PbsUser projectLead = osUserToPbsUser(osuser);
        addLeadPbs(projectLead, projectLeadPbsField, issue, userPbs);

        Collection<PbsUser> techLeads = getProjectTechLeads(issue.getProjectObject());
        for (PbsUser user: techLeads) {
            addLeadPbs(user, projectTechLeadPbsField, issue, userPbs);            
        }
    }

    private Collection<PbsUser> getProjectTechLeads(Project project) {
        Collection<PbsUser> techLeads = projectToTechLeads.get(project);
        if (techLeads == null) {
            techLeads = new ArrayList<PbsUser>();
            projectToTechLeads.put(project, techLeads);
            if (techLeadRole != null) {
                Collection<User> allUsers = userUtil.getAllUsersInGroupNames(Arrays.asList(new String[] {"jira-users"}));
                for (User user: allUsers) {
                    if (projectRoleManager.isUserInProjectRole(user, techLeadRole, project)) {
                        techLeads.add(osUserToPbsUser(user));
                    }
                }
            }
        }
        return techLeads;
    }

    private void addLeadPbs(PbsUser leadUser,
            CustomField leadPbsField, Issue issue, Map<PbsUser, Double> userPbs) {
        double pbsAdd = getPbs(issue, leadPbsField);
        Double pbs = userPbs.get(leadUser);
        if (pbs != null) {
            pbsAdd += pbs; 
        }
        userPbs.put(leadUser, pbsAdd);
    }

    private double getPbs(Issue issue, CustomField pbsField) {
        Double pbs = (Double) issue.getCustomFieldValue(pbsField);
        if (pbs ==  null) {
            pbs = 0D;
        }
        return pbs;
    }

    private void setupPbsUsers() {
        String metaProjectName = getProperty(PROPERTY_PREFIX + "metaproject", "metaproject");
        Project metaProject = projectManager.getProjectObjByName(metaProjectName);
        auditLeadUser = setupPbsUser(metaProject, "audit.lead.username");
        itLeadUser = setupPbsUser(metaProject, "it.lead.username");
    }

    private PbsUser setupPbsUser(Project metaProject, String metaName) {
        String username = getMeta(metaProject, metaName);
        if (username == null || username.isEmpty()) {
            throw new RuntimeException("Meta '" + metaName + "' is not set");
        }
        
        User osuser = userUtil.getUserObject(username);
        if (osuser == null) {
            throw new RuntimeException("User '" + username + "' does not exist");
        }
        return osUserToPbsUser(osuser);
    }

    private String getMeta(Project project, String name) {
        String value = metadataService.getMetadataValue(project, name);
        if (value == null) {
            throw new RuntimeException("Meta '" + name + "' is not set");
        }
        return value;
    }

    private void setupFields() {
        // TODO: instanceOf
        pbsGroupToField.put(SYS_ANALYSTS_PBS_GRP,
                (CustomField) getField("pbs_analyitcs", "customfield_11503")); 
        pbsGroupToField.put(TEST_ENG_PBS_GRP,
                (CustomField) getField("pbs_audit", "customfield_11504"));

        auditLeadPbsField = (CustomField) getField("pbs_audit_lead", "customfield_11510"); 

        pbsGroupToField.put(DESIGNERS_PBS_GRP,
                (CustomField) getField("pbs_design", "customfield_11506")); 
        pbsGroupToField.put(DEV_PBS_GRP,
                (CustomField) getField("pbs_developer", "customfield_11505")); 
        pbsGroupToField.put(HTML_CODERS_PBS_GRP,
                (CustomField) getField("pbs_html", "customfield_11507"));

        itLeadPbsField = (CustomField) getField("pbs_it_lead", "customfield_11509");
        projectLeadPbsField = (CustomField) getField("pbs_project_lead", "customfield_11512");
        projectTechLeadPbsField = (CustomField) getField("pbs_project_tech_lead", "customfield_11511"); 

        pbsGroupToField.put(COPYWRITERS_PBS_GRP,
                (CustomField) getField("pbs_type", "customfield_10000")); // 11508

        billingStatusField = (CustomField) getField("billing_status", "customfield_11000"); // 10007
    }

    private Field getField(String name, String defaultFieldId) {
        String fieldId = getProperty(PROPERTY_PREFIX + name + "_customfield", defaultFieldId);
        Field field = fieldManager.getField(fieldId);
        if (field == null) {
            throw new RuntimeException(name + " field does not exist, id:" + fieldId);
        }
        return field;
    }

    private String getProperty(String name, String defaultValue) {
        String value = applicationProperties.getDefaultBackedString(name);
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }

    private String userToPbsGroup(String username) {
        Set<String> userGroups;
        userGroups = userUtil.getGroupNamesForUser(username);
        for (String userGroup : userGroups) {
            if (SYS_ANALYSTS_JIRA_GRP.equals(userGroup)) {
                return SYS_ANALYSTS_PBS_GRP;
            } else if (TEST_ENG_JIRA_GRP.equals(userGroup)) {
                return TEST_ENG_PBS_GRP;
            } else if (DESIGNERS_JIRA_GRP.equals(userGroup)) {
                return DESIGNERS_PBS_GRP;
            } else if (DEV_JIRA_GRP.equals(userGroup)
                    || JR_DEV_JIRA_GRP.equals(userGroup)) {
                return DEV_PBS_GRP;
            } else if (HTML_CODERS_JIRA_GRP.equals(userGroup)) {
                return HTML_CODERS_PBS_GRP;
            } else if (COPYWRITERS_JIRA_GRP.equals(userGroup)) {
                return COPYWRITERS_PBS_GRP;
            }
        }
        log.warn("Unknown group for username:  " + username + ", gorups: " + userGroups.toString());
        return UNKNOWN_PBS_GRP;
    }

    public String generateReportHtml(ProjectActionSupport action, Map params)
            throws Exception {
        User remoteUser = authenticationContext.getLoggedInUser();
        I18nBean i18nBean = new I18nBean(remoteUser);
        TimeZone timezone = timeZoneManager.getLoggedInUserTimeZone();
        
        Date endDate = Pivot.getEndDate(params, i18nBean, timezone);
        Date startDate = Pivot.getStartDate(params, i18nBean, endDate, timezone);

        getTimeSpents(remoteUser, startDate, endDate, null, null, null, false);

        DateFieldFormat  dateFieldFormat = new DateFieldFormatImpl(dateTimeFormatterFactory);
        DatePickerConverter dpc = new DatePickerConverterImpl(authenticationContext, dateFieldFormat);
        Map<String, Object> velocityParams = new HashMap<String, Object>();
        velocityParams.put("startDate", dpc.getString(startDate));
        Calendar calendarDate = Calendar.getInstance();
        calendarDate.setTime(endDate);
        // timesheet report will add 1 day
        calendarDate.add(Calendar.DAY_OF_YEAR, -1); 
        velocityParams.put("endDate", dpc.getString(calendarDate.getTime()));
        velocityParams.put("outlookDate", dateTimeFormatterFactory.
                formatter().withStyle(DateTimeStyle.DATE).forLoggedInUser());
        velocityParams.put("fieldVisibility", fieldVisibilityManager);
        velocityParams.put("textUtil", new TextUtil(i18nBean, timezone));
        velocityParams.put("workedIssues", workedUsersOrig);
        velocityParams.put("workedUsers", workedTotal);
        velocityParams.put("pbsUsers", pbsUsers);
        return descriptor.getHtml("view", velocityParams);
    }

    // Validate the parameters set by the user.
    public void validate(ProjectActionSupport action, Map params) {
        User remoteUser = authenticationContext.getLoggedInUser();
        if (remoteUser == null) {
            action.addError("projectid", action
                    .getText("report.pbs.login"));
        }
        I18nHelper i18nBean = new I18nBean(remoteUser);

        Date startDate = ParameterUtils.getDateParam(params, "startDate",
                i18nBean.getLocale());
        Date endDate = ParameterUtils.getDateParam(params, "endDate",
                i18nBean.getLocale());

        // The end date must be after the start date
        if (startDate != null && endDate != null
                && endDate.before(startDate)) {
            action.addError("endDate", action
                    .getText("report.pbs.before.startdate"));
        }
    }

}
