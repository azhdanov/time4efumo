package com.fdu.jira.util;

public class PbsUser extends MyUser {
    private String pbsGroup;

    public PbsUser(String name, String fullName, String pbsGroup) {
        super(name, fullName);
        this.pbsGroup = pbsGroup;
    }

    public String getPbsGroup() {
        return pbsGroup;
    }

    public void setPbsGroup(String pbsGroup) {
        this.pbsGroup = pbsGroup;
    }


}
