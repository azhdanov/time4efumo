package com.fdu.jira.plugin.report.timesheet;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fdu.jira.util.TextUtil;

public class ProjectValuesGenerator extends  com.atlassian.jira.portal.ProjectValuesGenerator{

	public Map<String, String> getValues(Map arg0) {
        Map<String, String> values = super.getValues(arg0);
        Map<String, String> result = new LinkedHashMap<String, String>();
        result.put("", "All Projects");
        for (Iterator<String> i =  values.keySet().iterator(); i.hasNext();) {
            String key = i.next();
            String value = values.get(key);
            result.put(key,  TextUtil.getUnquotedString(value));
        }
        return result;
    }
}

