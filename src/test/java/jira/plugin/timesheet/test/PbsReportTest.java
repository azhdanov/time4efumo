package jira.plugin.timesheet.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
 
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.ofbiz.core.entity.GenericValue;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import jira.plugin.report.pbs.PbsReport;
 
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.timezone.TimeZoneManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.fdu.jira.util.PbsUser;
import com.osoboo.jira.metadata.MetadataService;

/**
 * 
 * @author Andriy Zhdanov (azhdanov@gmail.com)
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ComponentManager.class)
public class PbsReportTest {
    ProjectManager projectManager;
    WorklogManager worklogManager;
    ProjectRoleManager projectRoleManager;
    UserUtil userUtil;
    MetadataService metadataService;
    PbsReport report;

    CustomField pbsField;
    CustomField billingStatusField;

    @Before
    public void setup() {
        JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);
        projectManager = mock(ProjectManager.class);
        worklogManager = mock(WorklogManager.class);
        projectRoleManager = mock(ProjectRoleManager.class);
        userUtil = mock(UserUtil.class);
        TimeZoneManager timeZoneManager = mock(TimeZoneManager.class);
        metadataService = mock(MetadataService.class);
        FieldManager fieldManager = mock(FieldManager.class);
        pbsField = mock(CustomField.class);
        billingStatusField = mock(CustomField.class);
        when(fieldManager.getField(anyString())).thenAnswer(new Answer<CustomField>() {
            public CustomField answer(InvocationOnMock invocation) throws Throwable {
                if ("customfield_11000".equals(invocation.getArguments()[0])) {
                    return billingStatusField;
                } else {
                    return pbsField;
                    
                }
            }
        });
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);

        PowerMockito.mockStatic(ComponentManager.class);
        when(ComponentManager.getOSGiComponentInstanceOfType(MetadataService.class)).thenReturn(metadataService);

        DateTimeFormatterFactory dateTimeFormatterFactory = mock(DateTimeFormatterFactory.class);
        FieldVisibilityManager fieldVisibilityManager = mock(FieldVisibilityManager.class);

        report = new PbsReport(authenticationContext, projectManager,
                worklogManager, projectRoleManager, userUtil, timeZoneManager,
                fieldManager, applicationProperties, dateTimeFormatterFactory,
                fieldVisibilityManager);
    }

    @Test
    public void testReport() {
        SortedSet<User> allUsers = new TreeSet<User>();
        User uJohn = MockUtils.createUser("john", PbsReport.DEV_JIRA_GRP, userUtil, allUsers); 
        User uJoe = MockUtils.createUser("joe", PbsReport.JR_DEV_JIRA_GRP, userUtil, allUsers); 
        User uMia = MockUtils.createUser("mia", PbsReport.DESIGNERS_JIRA_GRP, userUtil, allUsers); 
        User uFoo = MockUtils.createUser("foo", PbsReport.SYS_ANALYSTS_JIRA_GRP, userUtil, allUsers); 
        User uBar = MockUtils.createUser("bar", PbsReport.TEST_ENG_JIRA_GRP, userUtil, allUsers); 
        User uLeo = MockUtils.createUser("leo", PbsReport.TEST_ENG_JIRA_GRP, userUtil, allUsers);
        List<String> anyGroupNames = anyObject();
        when(userUtil.getAllUsersInGroupNames(anyGroupNames)).thenReturn(allUsers);
        
        Project p = MockUtils.createProject(1, "Sample Project", "SAMPLE", uJohn);
        
        when(projectManager.getProjectObjByName("metaproject")).thenReturn(p);

        when(metadataService.getMetadataValue(p, "audit.lead.username")).thenReturn("bar");
        when(metadataService.getMetadataValue(p, "it.lead.username")).thenReturn("foo");

        ProjectRole projectRole = mock(ProjectRole.class);
        when(projectRoleManager.getProjectRole("Lead project PHP developer")).thenReturn(projectRole);
        when(projectRoleManager.isUserInProjectRole(uJohn, projectRole, p)).thenReturn(true);

        Issue i1 = MockUtils.createIssue(1L, "SAMPLE-1", p, pbsField, 2, billingStatusField, "agreement", null);
        Issue i2 = MockUtils.createIssue(1L, "SAMPLE-2", p, pbsField, 2, null, null, i1);
        Issue i3 = MockUtils.createIssue(1L, "SAMPLE-3", p, pbsField, 2, null, null, i1);

        Worklog wJohn1 = MockUtils.createWorklog(10, uJohn, i1, 1, worklogManager);
        Worklog wJohn2 = MockUtils.createWorklog(11, uJohn, i2, 1, worklogManager);
        Worklog wJoe1 = MockUtils.createWorklog(20, uJoe, i1, 2, worklogManager);
        Worklog wJoe3 = MockUtils.createWorklog(21, uJoe, i3, 2, worklogManager);
        Worklog wMia1 = MockUtils.createWorklog(30, uMia, i1, 1, worklogManager);
        Worklog wMia3 = MockUtils.createWorklog(31, uMia, i3, 4, worklogManager);
        Worklog wFoo1 = MockUtils.createWorklog(40, uFoo, i1, 2, worklogManager);
        Worklog wFoo2 = MockUtils.createWorklog(41, uFoo, i2, 1, worklogManager);
        Worklog wFoo3 = MockUtils.createWorklog(42, uFoo, i3, 1, worklogManager);
        Worklog wBar1 = MockUtils.createWorklog(50, uBar, i1, 2, worklogManager);
        Worklog wBar2 = MockUtils.createWorklog(51, uBar, i2, 3, worklogManager);
        Worklog wBar3 = MockUtils.createWorklog(52, uBar, i3, 4, worklogManager);
        Worklog wLeo1 = MockUtils.createWorklog(60, uLeo, i1, 2, worklogManager);
        Worklog wLeo2 = MockUtils.createWorklog(61, uLeo, i2, 1, worklogManager);
        Worklog wLeo3 = MockUtils.createWorklog(62, uLeo, i3, 1, worklogManager);

        List<GenericValue> worklogs = new ArrayList<GenericValue>();
        worklogs.add(MockUtils.createGenericValue(wJohn1));
        worklogs.add(MockUtils.createGenericValue(wJohn2));
        worklogs.add(MockUtils.createGenericValue(wJoe1));
        worklogs.add(MockUtils.createGenericValue(wJoe3));
        worklogs.add(MockUtils.createGenericValue(wMia1));
        worklogs.add(MockUtils.createGenericValue(wMia3));
        worklogs.add(MockUtils.createGenericValue(wFoo1));
        worklogs.add(MockUtils.createGenericValue(wFoo2));
        worklogs.add(MockUtils.createGenericValue(wFoo3));
        worklogs.add(MockUtils.createGenericValue(wBar1));
        worklogs.add(MockUtils.createGenericValue(wBar2));
        worklogs.add(MockUtils.createGenericValue(wBar3));
        worklogs.add(MockUtils.createGenericValue(wLeo1));
        worklogs.add(MockUtils.createGenericValue(wLeo2));
        worklogs.add(MockUtils.createGenericValue(wLeo3));
        report.processWorklogs(worklogs);

        System.out.print("Issue\\User | ");
        for (PbsUser user : report.workedTotal.keySet()) {
            System.out.print(user.getName() + " | ");
        }
        System.out.println();
        for (Issue issue : report.workedUsersOrig.keySet()) {
            System.out.print(issue.getKey() + "   | ");
            Map<PbsUser, Long> userWorkLog = report.workedUsersOrig.get(issue);
            for (PbsUser user : report.workedTotal.keySet()) {
                Long timespent = userWorkLog.get(user);
                if (timespent == null) {
                    timespent = 0L;
                }
                System.out.print("" + timespent / 3600 + "   | ");
            }            
            System.out.println();
        }
        System.out.println();

        System.out.print("Issue\\User | ");
        for (PbsUser user : report.workedTotal.keySet()) {
            System.out.print(user.getName() + " | ");
        }
        System.out.println();
        for (Issue issue : report.workedUsersOrig.keySet()) {
            System.out.print(issue.getKey() + "   | ");
            Map<PbsUser, Double> userPbs = report.pbsUsers.get(issue);
            for (PbsUser user : report.workedTotal.keySet()) {
                Double pbs = userPbs != null ? userPbs.get(user) : 0d;
                if (pbs == null) {
                    pbs = 0d;
                }
                System.out.print("" + String.format("%.1f", pbs) + " | ");
            }            
            System.out.println();
        }
    }
 
}