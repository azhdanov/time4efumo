package jira.plugin.timesheet.test;


import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.SortedSet;
import java.util.TreeSet;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.worklog.Worklog;
import com.atlassian.jira.issue.worklog.WorklogManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.util.UserUtil;


public class MockUtils {

   public static Project createProject(long id, String name, String key, User lead) {
       Project project = mock(Project.class);

       when(project.getId()).thenReturn(id);
       when(project.getName()).thenReturn(name);
       when(project.getKey()).thenReturn(key);
       when(project.getLeadUser()).thenReturn(lead);

       return project;
   }

   public static Issue createIssue(long id, String key, Project p, CustomField pbsCF, double pbs, CustomField billingStatusCF, String billingStatus, Issue parent) {
       Issue issue = mock(Issue.class);

       when(issue.getId()).thenReturn(id);
       when(issue.getKey()).thenReturn(key);
       when(issue.getProjectObject()).thenReturn(p);
       when(issue.getCustomFieldValue(pbsCF)).thenReturn(pbs);
       when(issue.getCustomFieldValue(billingStatusCF)).thenReturn(billingStatus);

       if (parent != null) {
           when(issue.isSubTask()).thenReturn(true);
           when(issue.getParentObject()).thenReturn(parent);
       }

       return issue;
   }

   public static Worklog createWorklog(long id, User u, Issue issue, long hours,
           WorklogManager worklogManager) {
       Worklog worklog = mock(Worklog.class);

       when(worklog.getId()).thenReturn(id);
       String uName = u.getName();
       when(worklog.getAuthor()).thenReturn(uName);
       when(worklog.getIssue()).thenReturn(issue);
       when(worklog.getTimeSpent()).thenReturn(hours * 3600);
       when(worklogManager.getById(id)).thenReturn(worklog);

       return worklog;
   }

    public static GenericValue createGenericValue(Worklog worklog) {
        GenericValue gv = mock(GenericValue.class);
        Long id = worklog.getId();
        when(gv.getLong("id")).thenReturn(id);
        String author = worklog.getAuthor();
        when(gv.get("author")).thenReturn(author);
        return gv;
    }

    public static User createUser(String name, String groupName, UserUtil userUtil, SortedSet<User> allUsers) {
        User user = mock(User.class);

        when(user.getName()).thenReturn(name);
        when(user.getDisplayName()).thenReturn(name.toUpperCase());
        when(userUtil.getUserObject(name)).thenReturn(user);
        SortedSet<String> g = new TreeSet<String>();
        g.add(groupName);
        when(userUtil.getGroupNamesForUser(name)).thenReturn(g);

        allUsers.add(user);

        return user;
    }

}

