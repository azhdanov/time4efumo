package it.jira.plugin.timesheet.test;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.meterware.httpunit.WebResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;

public class TestPivotReport extends FuncTestCase {

    private Client client;

    public void setUpTest() {
        //administration.restoreData("example.xml");
        client = Client.create();
    }

    public void testSomething() {
        final WebResource resource = client.resource("http://localhost:2990/jira");

        final WebResponse response = resource.get(new GenericType<WebResponse>() {});
        assertNotNull(response);
    }

}
